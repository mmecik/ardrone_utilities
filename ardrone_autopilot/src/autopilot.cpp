#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>
#include <stdio.h>
#include <std_msgs/Empty.h>
#include <sensor_msgs/Joy.h>
#include <ardrone_autonomy/LedAnim.h>
#include <ardrone_autonomy/CamSelect.h>

//Orangenes Blinken für die LED-Animation
#define BLINK_ORANGE 3

// Globale Variablen
bool autopilot = false;
bool autolanding = false;
bool markerFound = false;
bool auto_hover = false;
bool hoverLeds = false;
bool timerIsRunning = false;

// Publisher für das Senden eines Lande-Befehls
ros::Publisher ardrone_landing_pub;

ros::Publisher ardrone_hover_pub;

//Subscriber für das Horchen von Joystick-Eingaben
ros::Subscriber sub_joy_listener;



//Prototyp der Joystick-Callback-Funktion für das Landen der Drohne
void joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg);


//Implementierung der Joystick-Callback-Funktion für das Landen der Drohne
void joyCallback( const sensor_msgs::Joy::ConstPtr& joy_msg){


	 //Taste 10 Logitech-Controller
	 int btn_autolanding = joy_msg->buttons[9];

	 //Taste 8 Logitech-Controller
	 int btn_hover = joy_msg->buttons[7];

	 //Ein-/Ausschalten vom Autolanden
	 if ( (btn_autolanding == 1) && (autopilot == false) ){
		 ROS_INFO("%s", "Auto landing enabled...");
		 autopilot = true;
		 autolanding = false;
		 markerFound = false;
	 } else if ( btn_autolanding == 1 ){
		 ROS_INFO("%s", "Auto landing disabled...");
		 autopilot = false;;
		 autolanding = false;
		 markerFound = false;
	 }

	 // Ein-/Ausschalten vom Hovering
	 if ( (btn_hover == 1) && (auto_hover == false) ){
		 ROS_INFO("Hover an");
		 auto_hover = true;
	 } else if ( btn_hover == 1 ) {
		 ROS_INFO("Hover aus");
		 auto_hover = false;
		 hoverLeds = false;
	 }
}


//Timer-Callback, zum Neustarten der LED-Animation
void callback(const ros::TimerEvent& event)
{
	hoverLeds = true;
	timerIsRunning = true;
}

int main(int argc, char** argv) {
	ros::init(argc, argv, "IMR_Auto_Landing");

	ros::NodeHandle nh;
	ros::Subscriber ar_pose_sub;


	//Service für die LED-Animation
	ros::ServiceClient ledClient = nh.serviceClient<ardrone_autonomy::LedAnim>("/ardrone/setledanimation");

	// Led Service Message
	ardrone_autonomy::LedAnim ledMsg;
	ledMsg.request.type = BLINK_ORANGE; //Orange blinkend
	ledMsg.request.freq = 6.0; //Frequenz in Hz
	ledMsg.request.duration = 1; //1 Sekunde


	//Leere Nachricht
	std_msgs::Empty empty;

	//Transform-Listener für die AR-Marker 8,9 und 10
	tf::TransformListener listener_armarker8, listener_armarker9,listener_armarker10;

	//TF's für die jeweiligen AR-Marker
	tf::StampedTransform transform1, transform2, transform3;

	//Frequenz, mit der geprüft wird ob die AR-Marker vorhanden sind
	//ros::Rate rate(AR_FREQUENCY);

	sub_joy_listener = nh.subscribe<sensor_msgs::Joy>("joy",1000, &joyCallback);

	//Publisher, der den Befehl für das Landen der Drohne bereitstellt;
	ardrone_landing_pub = nh.advertise<std_msgs::Empty>("ardrone/land", 1);

	//Publisher, der den Befehl des Hoverns sendet
	ardrone_hover_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel",1000);

	// Schleifen-Geschwindigkeit von ros::spin
	ros::Rate rate(50.0);

	// Timer, für das neu
	ros::Timer timer = nh.createTimer(ros::Duration(0.1), callback);

	while ( ros::ok() ) {

			if ( auto_hover == false ){

				if ( timerIsRunning == true ){
					timer.stop();
					timerIsRunning = false;
				}

				//ROS_INFO("Autopilot %d",autopilot);
				if ( (autopilot == true) && (markerFound == false)){
					tf::StampedTransform transform;

					//Suche nach Ar-Marker 8
					try {
						listener_armarker8.lookupTransform("/ardrone_base_link",
								"/ar_marker_8", ros::Time(0), transform1);
						ROS_INFO("%s", "marker8 found");
						markerFound = true;
					} catch (tf::TransformException ex) {
						ROS_ERROR("%s","marker8 not found");
					}

					//Suche nach Ar-Marker 9
					try {
						listener_armarker9.lookupTransform("/ardrone_base_link",
								"/ar_marker_9", ros::Time(0), transform2);
						ROS_INFO("%s", "marker9 found");
						markerFound = true;
					} catch (tf::TransformException ex) {
						ROS_ERROR("%s","marker9 not found");
					}

					//Suche nach Ar-Marker 10
					try {
						listener_armarker10.lookupTransform("/ardrone_base_link",
								"/ar_marker_10", ros::Time(0), transform3);
						ROS_INFO("%s", "marker10 found");
						markerFound = true;
					} catch (tf::TransformException ex) {
						ROS_ERROR("%s", "marker10 not found");
					}

				//Wenn Marker gefunden wurde, dann soll die Drohne landen
				} else if (markerFound == true){
					ROS_INFO("%s", "Auto landing...");
					ardrone_landing_pub.publish(empty);

					ROS_INFO("ARdrone landed");

					autopilot = false;
					markerFound = false;
				}

			//Starten des LED-Animations-Timers
			} else {
				timer.start();
			}

			// Prüfen, ob LED-Animation eingeschaltet werden soll
			if ( hoverLeds == true ){
				ledClient.call(ledMsg);
				hoverLeds = false;
			}

		ros::spinOnce();
		rate.sleep();
	}
	return 0;
}
