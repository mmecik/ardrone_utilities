#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Empty.h>

#define DEBUG

class JoystickInterfaceNode
{


public:
  ros::NodeHandle nh;
  ros::Subscriber sub_joy_listener;
  ros::Publisher  pub_ardrone_twist;
  ros::Publisher  pub_ardrone_takeoff;
  ros::Publisher  pub_ardrone_land;
  ros::Publisher  pub_ardrone_reset;

  JoystickInterfaceNode();
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg);

private:

};

JoystickInterfaceNode::JoystickInterfaceNode(){
	//Publisher
	pub_ardrone_twist = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);
	pub_ardrone_takeoff = nh.advertise<std_msgs::Empty>("/ardrone/takeoff", 1, true);
	pub_ardrone_land = nh.advertise<std_msgs::Empty>("/ardrone/land", 1, true);
	pub_ardrone_reset = nh.advertise<std_msgs::Empty>("/ardrone/reset", 1, true);

	//Subscriber
	sub_joy_listener = nh.subscribe<sensor_msgs::Joy>("joy",1000, &JoystickInterfaceNode::joyCallback, this);
}

void JoystickInterfaceNode::joyCallback( const sensor_msgs::Joy::ConstPtr& joy_msg){
	geometry_msgs::Twist twist;

	//Achsen
	float x_axis = joy_msg->axes[1];
	float y_axis = joy_msg->axes[0];
	float altitude = joy_msg->axes[3];
	float rotation = joy_msg->axes[2];

	//Zwischenspeichern der Buttons
	int btn0 = joy_msg->buttons[0]; 	//frei
	int btn1 = joy_msg->buttons[1];		//frei
	int btn2 = joy_msg->buttons[2]; 	//frei
	int btn3 = joy_msg->buttons[3]; 	//frei
	int btn4 = joy_msg->buttons[4]; 	//frei
	int btn5 = joy_msg->buttons[5];		//frei
	int btn6 = joy_msg->buttons[6]; 	//Takeoff Drohne
	int btn7 = joy_msg->buttons[7]; 	//frei
	int btn8 = joy_msg->buttons[8]; 	//Landen Drohne
	int btn9 = joy_msg->buttons[9]; 	//frei
	int btn10 = joy_msg->buttons[10]; 	//Notaus Drohne

	// Mapping
	int takeoff_drohne = btn6;
	int landen_drohne = btn8;
	int notaus_drohne = btn10;

	//Empty Message
	std_msgs::Empty empty;

	//	------------------------------
	//	Dokumentation
	//	------------------------------
	// 	See documentation http://ardrone-autonomy.readthedocs.org/en/latest/commands.html
	//	------------------------------

	twist.linear.x = x_axis;
	twist.linear.y = y_axis;
	twist.linear.z = altitude;
	twist.angular.z = rotation;

	// Publishen eines Start-Befehls
	if (takeoff_drohne != 0){
		pub_ardrone_takeoff.publish(empty);
	}

	// Publishen eines Lande-Befehls
	if (landen_drohne != 0){
		pub_ardrone_land.publish(empty);
	}

	// Publishen eines Notaus-Befehls
	if (notaus_drohne != 0){
		pub_ardrone_reset.publish(empty);
	}

	pub_ardrone_twist.publish(twist);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "joystickInterface_node");
  JoystickInterfaceNode jin;
  ros::spin();
  return 0;
}
